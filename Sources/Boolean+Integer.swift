//
//  Boolean+Integer.swift
//  KnapSackProblemSolver
//
//  Created by Kurt Niemi on 3/12/16.
//  Copyright © 2016 Kurt Niemi. All rights reserved.
//

import Foundation

// Extension to Bool to allow us to create it from an Int data type
extension Bool {
    init<T : IntegerType>(_ integer: T){
        self.init(integer != 0)
    }
}
