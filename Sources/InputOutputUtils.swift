//
//  output.swift
//  KnapSackProblemSolver
//
//  Created by Kurt Niemi on 3/12/16.
//  Copyright © 2016 Kurt Niemi. All rights reserved.
//

import Foundation

// Helper method to output solution results
func outputResults(input:KnapSackGAProblemInput, output:KnapSackGAProblemOutput)
{
    let result = output.solution
    let numberOfGenerations = output.numGenerationsToSolution

    print("Algorithm took \(numberOfGenerations) generations to get results")
    for i in 0..<input.numberOfItems
    {
        if result[i]
        {
            print("Item \(i) with Value \(input.valueOfItems[i]) and Weight \(input.weightOfItems[i]) is in knapsack.")
        }
        else
        {
            print("Item \(i) with Value \(input.valueOfItems[i]) and Weight \(input.weightOfItems[i]) is not in knapsack.")
        }
    }
    
}

// Helper method to prompt for user to enter in an Integer
func promptForInteger(prompt:String) -> Int
{
    print(prompt)
    
    var result:Int!
    
    repeat
    {
        if let input = readLine(stripNewline: true)
        {
            result = Int(input)
        }
        
        if (result == nil)
        {
            print("Invalid input")
        }
        
    } while (result == nil)
    
    return result
}

// Helper method to prompt for user to enter in an Double value
func promptForDouble(prompt:String) -> Double
{
    print(prompt)
    
    var result:Double!
    
    repeat
    {
        if let input = readLine(stripNewline: true)
        {
            result = Double(input)
        }
        
        if (result == nil)
        {
            print("Invalid input")
        }
        
    } while (result == nil)
    
    return result
}


// Helper method to prompt for user to enter in an array of Int values
func promptIntegerArray(prompt:String,numItems:Int) -> [Int]
{
    var output:[Int] = [Int]()
    for i in 0..<numItems
    {
        let input = promptForInteger("\(prompt) #\(i)")
        output.append(input)
    }
    
    return output
}
