//
//  KnapSackGAProblemSolver.swift
//  KnapSackProblemSolver
//
//  Created by Kurt Niemi on 3/6/16.
//  Copyright © 2016 Kurt Niemi. All rights reserved.
//

import Foundation

// Struct that contains GA problem that we are trying to find optimal solution for
struct KnapSackGAProblemInput
{
    var numberOfItems:Int           // Number of items we would like to put in Knapsack
    var maxCapacity:Int             // Max Volume Capacity
    var valueOfItems:[Int]          // Array with value of each item
    var weightOfItems:[Int]         // Array with weight (volume) of each time
    var populationSize:Int          // Number of GA chromosomes
    var maxGenerations:Int          // Max number of generations to find solution for
    var crossoverProbability:Double // Probability that we do a crossover on a given chromsome
    var mutationProbability:Double  // Probability that we mutate individual bits within chromosomes
}

// Struct that contains solution output
struct KnapSackGAProblemOutput
{
   var solution:[Bool]			// Solution
   var numGenerationsToSolution:Int	// Number of Generations to find solution
}

class KnapSackGAProblemSolver
{
    var input:KnapSackGAProblemInput        // Property for maintaning problem input
    var chromosomes:[[Bool]] = [[Bool]]()   // Array of current generation chromosomes

    // Class initializer
    init (input:KnapSackGAProblemInput)
    {
        self.input = input
    }
    
    // Method to find optimal GA solution.  This method returns a KnapSackGAProblemOutput struct with the solution.
    func solve() -> KnapSackGAProblemOutput
    {
        // First we generate a random initial population of chromosomes
        chromosomes = generateRandomInitialPopulation()
        
        // We calculate the overall fitness and volume for all the chromosomes
        var (fitness, volume) = calculateFitnessAndVolume()
        var generationNumber = 1;

        // We will create subseuqent generations, until either we have exceeded the maximum generations OR
        // the results have converged.   Convergence is defined as 90% of the chromosomes having the 
        // same fitness value.
        while (generationNumber < input.maxGenerations && !resultsConverged(fitness, volume:volume))
        {
            // Create empty array for next generation
            var nextGeneration = [[Bool]]()

            // For each chromosome in current generation determine if we are going to clone/copy or perform
            // crossover with 2 randomly selected chromosomes, and then apply mutation.
            for i in 0..<input.populationSize
            {
                var childChromosome:[Bool] = [Bool]()
                
                if (input.crossoverProbability > cs_arc4random_probability())
                {
                    // Perform Crossover - selecting 2 parents using Roulette selection
                    let parent1 = rouletteSelection(fitness)
                    let parent2 = rouletteSelection(fitness)
                    
                    childChromosome = performCrossOver(chromosomes[parent1], parent2: chromosomes[parent2])
                }
                else
                {
                    // Copy/clone chromosome to next generation
                    childChromosome = chromosomes[i]
                }
                
                // Apply mutation
                childChromosome = performMutation(childChromosome, mutationProbability:input.mutationProbability)
                
                // Add new chromsome to next generation
                nextGeneration.append(childChromosome)
            }

            // Set new generation as current generation, and increment generation number
            chromosomes = nextGeneration
            generationNumber = generationNumber + 1;

            // Calculate new fitness values for chromosomes
            (fitness, volume) = calculateFitnessAndVolume()
        }
        
        // Get first best solution
        let solutionIndex = getFirstBestSolutionIndex(fitness)
    
	// Create output struct
	let output = KnapSackGAProblemOutput(solution:chromosomes[solutionIndex],numGenerationsToSolution:generationNumber)
	return output 
    }
    
    // Method to perform Crossover on 2 chromosomes.  Method returns the results
    func performCrossOver(parent1:[Bool], parent2:[Bool]) -> [Bool]
    {
        // Randomly choose starting point of parent2 to merge
        let startPoint = Int(cs_arc4random_uniform(UInt32(parent1.count)))
        
        // Randomly choose length of parent2 to merge from starting point
        let length = Int(cs_arc4random_uniform(UInt32(parent1.count-startPoint+1)))
        let endPoint = startPoint + length
        
        var crossOverChromosome = [Bool]()

        for i in 0..<parent1.count
        {
            if i >= startPoint && i < endPoint
            {
                crossOverChromosome.append(parent2[i])
            }
            else
            {
                crossOverChromosome.append(parent1[i])
            }
        }
        
        return crossOverChromosome
    }
    
    // Method ot perform mutation on chromsome array.   Modified chromosome is returned
    func performMutation(chromosome:[Bool], mutationProbability:Double) -> [Bool]
    {
        var mutatedChromosome = chromosome
        
        // Loop over each item in array and determine wheteher that element should be mutated
        for i in 0..<chromosome.count
        {
            if (mutationProbability > cs_arc4random_probability())
            {
                mutatedChromosome[i] = !mutatedChromosome[i]
            }
        }
        
        return mutatedChromosome
    }
    
    // Algorithm to perform roulette style selection.  The array of fitness values is passed in
    // and we return the array index that corresponds to the chromosome.
    func rouletteSelection(fitness:[Int]) -> Int
    {
        // Calculate sum of total fitness
        let totalFitness = fitness.reduce(0, combine: +)
        
        // Randomly generate a value between 0 and totalFitness
        let rouletteValue = Int(cs_arc4random_uniform(UInt32(totalFitness)))

        // Loop over array, keeping track of accumulated fitness.  When this value is more
        // than the random value we have found the corresponding element.
        var accumulatedFitness = 0
        for i in 0..<fitness.count
        {
            accumulatedFitness += fitness[i]
            
            if (accumulatedFitness > rouletteValue)
            {
                // We've found the element that corresponds to the roulette value
                return i
            }
        }
        
        // If we haven't returned, it must be the last element
        return fitness.count-1
        
    }
    
    // Method to find the first best (optimal) solution index given an array of fitness values
    func getFirstBestSolutionIndex(fitness:[Int]) -> Int
    {
        var maxItemFitnessIndex = -1
        
        for i in 0..<chromosomes.count
        {
            let chromosome = chromosomes[i]
            let fitness = calculateFitness(chromosome)
            
            if (fitness > maxItemFitnessIndex)
            {
                maxItemFitnessIndex = i
            }
        }
        
        return maxItemFitnessIndex
    }

    // Method to determine if results have converged
    func resultsConverged(fitness:[Int], volume:[Int]) -> Bool
    {
        // First we sort the fitness array
        var sortedFitness = fitness.sort()
        
        // Convergence is defined as 90% or more of items having same fitness value.  To do this we
        // need to keep track of our current fitness value (and evaluating whether that value causes us to
        // exceed the threshold.

        // The very first element in our sorted array, is our current fitness candidate.  The count is initialized to 1
        var currentFitness = sortedFitness[0]
        var currentFitnessCount = 1
        
        // Our current index is set to the 2nd item in the array
        var currentIndex = 1
        
        repeat
        {
            if sortedFitness[currentIndex] == currentFitness
            {
                // The current item in the array has the same fitness value - so we bump up the count
                currentFitnessCount = currentFitnessCount + 1

                // We check to see if we've exceeded the threshold
                if (Double(currentFitnessCount) / Double(sortedFitness.count) >= 0.90)
                {
                    return true
                }
            }
            else
            {
                // Current item is different.   We update the currentFitness value to the new value, and reset 
                // count back to 1
                currentFitness = sortedFitness[currentIndex]
                currentFitnessCount = 1
            }

            // Increment current index
            currentIndex = currentIndex + 1
            
        } while (currentIndex < sortedFitness.count)
        
        // Check to see if we've exceeeded the threshold
        if (Double(currentFitnessCount) / Double(sortedFitness.count) >= 0.90)
        {
            return true
        }

        // We did not exceed the threshold
        return false
    }
    
    // Method to calculate array of fitness and volume arrays
    func calculateFitnessAndVolume() -> (fitness:[Int], volume:[Int])
    {
        var fitnessArray = [Int]()
        var volumeArray = [Int]()
        
        for i in 0..<chromosomes.count
        {
            var chromosome = chromosomes[i]
            
            var volume = calculateVolume(chromosome)
            var fitness = calculateFitness(chromosome)

            // If our chromosome is invalid (ie all items cannot fit into knapsack we must
            // remove some)
            if (volume > input.maxCapacity) {
                removeRandomItemsFromKnapsack(&chromosome)
                chromosomes[i] = chromosome
                volume = calculateVolume(chromosome)
                fitness = calculateFitness(chromosome)
            }
            
            fitnessArray.append(fitness)
            volumeArray.append(volume)
        }
        
        return (fitnessArray,volumeArray)
    }

    // Method to remove a random item from the knapsack
    func removeRandomItemsFromKnapsack(chromosome:inout [Bool])
    {
        // While we could randomly remove any arbitrary item, it makes the most sense to either remove
        // the largest volume item (with the hopes that all the remaining items all fit) OR to remove the
        // smallest weighted items in an attempt to maximize the amount we fill

        // Boolean on whether we will be removing the max weighted item
        let removeMaxVolumeItems = Bool(Int(cs_arc4random_uniform(2)))

        // Call function to remove the item
        removeRandomItemsFromKnapsack(&chromosome,removeMaxVolumeItems: removeMaxVolumeItems)

        // Calculate new volume
        var volume = calculateVolume(chromosome)

        // Potentially have to repeat this removal process
        while (volume > input.maxCapacity) {
            removeRandomItemsFromKnapsack(&chromosome,removeMaxVolumeItems: removeMaxVolumeItems)
            volume = calculateVolume(chromosome)
        }
    }

    // Method that does work of removing a random item
    func removeRandomItemsFromKnapsack(chromosome:inout [Bool], removeMaxVolumeItems:Bool)
    {
        // We need to calculate the indexes that corresponds to the min and max weight
        var maxItemWeightIndex = -1
        var minItemWeightIndex = -1
        var maxItemWeight = -1
        var minItemWeight = 99999999
        
        for i in 0..<chromosome.count
        {
            if chromosome[i]
            {
                let itemWeight = input.weightOfItems[i]
                if (itemWeight > maxItemWeight)
                {
                    maxItemWeight = itemWeight
                    maxItemWeightIndex = i
                }
                
                if (itemWeight < minItemWeight)
                {
                    minItemWeight = itemWeight
                    minItemWeightIndex = i
                }
            }
        }
        
        // Set appropiate item to false
        if (removeMaxVolumeItems)
        {
            chromosome[maxItemWeightIndex] = false
        }
        else
        {
            chromosome[minItemWeightIndex] = false
        }
    }

    // Method to calcluate volume used by chromosome
    func calculateVolume(chromosome:[Bool]) -> Int
    {
        // Loop over all items in chromosome array
        var volume = 0
        for i in 0..<chromosome.count
        {
            // Add corresponding item weight in knapsack
            if chromosome[i]
            {
                volume += input.weightOfItems[i]
            }
        }
        return volume
    }

    // Method to calculate fitness of a chromosome
    func calculateFitness(chromosome:[Bool]) -> Int
    {
        // Loop over all items in chromsome array
        var fitness = 0
        for i in 0..<chromosome.count
        {
            // Add corresponding value in Knapsack if we have item
            if chromosome[i]
            {
                fitness += input.valueOfItems[i]
            }
        }
        return fitness
    }
    

    // Method to generate initial random populate of chromsomes.   This method returns an array of array of Boolean values
    func generateRandomInitialPopulation() -> [[Bool]]
    {
        var population = [[Bool]]()

        for _ in 0..<input.populationSize
        {
            var chromosome = [Bool]()
            
            for _ in 0..<input.numberOfItems
            {
                let random = Bool(Int(cs_arc4random_uniform(2)))
                chromosome.append(random)
            }
            
            population.append(chromosome)
        }
        return population
    }
    
}
