//
//  main.swift
//  KnapSackProblemSolver
//
//  Created by Kurt Niemi on 3/6/16.
//  Copyright © 2016 Kurt Niemi. All rights reserved.
//

import Foundation

// Prompt for inputs
let numberOfItems = promptForInteger("Number of Items:")
let maxCapacity = promptForInteger("Max Capacity of Knapskack:")
let valueOfItems = promptIntegerArray("Value of Items",numItems: numberOfItems)
let weightOfItems = promptIntegerArray("Weight of Items",numItems: numberOfItems)
let populationSize = promptForInteger("Population Size:")
let maxGenerations = promptForInteger("Max Generations:")
let crossOverProbability = promptForDouble("Crossover Probability (0-1.0)")
let mutationProbability = promptForDouble("Mutation Probability (0-1.0)")

// Create KnapSackGAProblemInput struct with user entered input
let input = KnapSackGAProblemInput(numberOfItems:numberOfItems, maxCapacity: maxCapacity, valueOfItems: valueOfItems,
weightOfItems: weightOfItems, populationSize: populationSize, maxGenerations: maxGenerations, crossoverProbability: crossOverProbability, mutationProbability: mutationProbability)

// Create KnapSackGAProblemSolver instance passing in input
let problemSolver = KnapSackGAProblemSolver(input: input)

// Solve problem
let output = problemSolver.solve()

// Output results
outputResults(input, output: output)


