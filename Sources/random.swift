//
//  random.swift
//  KnapSackProblemSolver
//
//  Created by Kurt Niemi on 3/10/16.
//  Copyright © 2016 Kurt Niemi. All rights reserved.
//

import Foundation

// Determine modules to import based on plaform
#if os(Linux)
    import Glibc
    import SwiftShims
#else
    import Darwin
#endif

// Cross-platform (Mac / Open Source Swift) arc4random_uniform
func cs_arc4random_uniform(upperBound: UInt32) -> UInt32 {
    #if os(Linux)
        return _swift_stdlib_arc4random_uniform(upperBound)
    #else
        return arc4random_uniform(upperBound)
    #endif
}

// Cross-platform (Mac / Open Source Swift) arc4random - returning value between 0 and 1
func cs_arc4random_probability() -> Double {
    #if os(Linux)
        return Double(_swift_stdlib_arc4random()) / Double(UINT32_MAX)
    #else
        return Double(arc4random()) / Double(UINT32_MAX)
    #endif
}
